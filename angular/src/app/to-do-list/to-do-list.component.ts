import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgModel } from '@angular/forms';
//
import { PersistanceService } from '../persistance.service';
import { Subscription } from 'rxjs/subscription';

@Component({
  selector: 'to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit, OnDestroy {
  /*tasks = [
    {name: 'buy Bitcoin', done: false}
  ]*/
  private tasks: { name: string, done: boolean , addedBy: string}[];
  private currentUser = localStorage.getItem("currentUser");
  private subscription: Subscription;

  constructor(private persistanceService: PersistanceService) { }

  ngOnInit() {
    this.subscription = this.persistanceService.getAllTasks()
    .subscribe(tasks => this.tasks = tasks);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe()
  }

  addTask(taskModel: NgModel) {
	  var tasks = JSON.parse(localStorage.getItem("tasks"));
	  const task = {name: taskModel.value, done: false, addedBy: this.currentUser};
	  tasks.push(task);
	  
	  
    /*const task = {name: taskModel.value, done: false, addedBy: this.currentUser};

    this.tasks.push(task);*/
    this.persistanceService.saveTask(task);
	this.saveTasks(tasks);

    taskModel.reset();
  }

  toogleTask(index) {
    this.tasks[index].done = !this.tasks[index].done;
	var tasks = JSON.parse(localStorage.getItem("tasks"));
	for (var i=0; i<tasks.length; ++i) {
		if (i==index) {
			tasks[i]=this.tasks[i];
			localStorage.setItem("tasks",JSON.stringify(tasks));
			break;
		}
	}
	
  }

  
  removeTask(index) {
	  var tasks = JSON.parse(localStorage.getItem("tasks"));
	  var realTasks=[];
	  //alert("eirugn");
	  for (var i=0; i<tasks.length; ++i) {
			//alert("eirsadugn");
		  if (i!=index) {
			  //alert("sajt");
		  realTasks.push(tasks[i]);
		  alert("hát, ezt bepusholtam");}
	  }
	  localStorage.setItem("tasks",JSON.stringify(realTasks));
    //this.tasks.splice(index,1)
	
  }

  saveTasks(tasks) {
    //localStorage.setItem('tasks', JSON.stringify([{name: "krumpli", done: true}]));
	localStorage.setItem('tasks', JSON.stringify(tasks));
    //this.persistanceService.saveTask(this.tasks);
  }
}
