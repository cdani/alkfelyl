import { Injectable } from '@angular/core';
//
import { Observable } from 'rxjs/observable';
import { of } from 'rxjs/observable/of';
import { HttpClient } from '@angular/common/http';

type ToDo = { name: string, done: boolean, addedBy: string };
type User = { username: string, password: string};

@Injectable()
export class PersistanceService {

  constructor(private http: HttpClient) { 


    if(!localStorage.getItem('tasks')) {
      localStorage.setItem('tasks', '[]');
    }
	if(!localStorage.getItem('users')) {
      localStorage.setItem('users', '[]');
    }
   }
  //
  getAllTasks(): Observable<ToDo[]> {
     const tasks = JSON.parse(localStorage.getItem('tasks'));
     return of(tasks);
    //return this.http.get<ToDo[]>('http://localhost:8080/api/to-do/all');
  }

  saveTask(task) {
    let tasks = JSON.parse(localStorage.getItem('tasks'));

    tasks.push(task);

    localStorage.setItem('tasks', JSON.stringify(tasks));
  }
  
	getAllUsers(): Observable<User[]> {
     const users = JSON.parse(localStorage.getItem('users'));

     return of(users);
    //return this.http.get<ToDo[]>('http://localhost:8080/api/to-do/all');
  }
	
	saveUsers(user) {
		let users = JSON.parse(localStorage.getItem('users'));
		//alert(users[0][0][0]);
		users.push(user);
		//users=users[0];
		
		localStorage.setItem('users', JSON.stringify(users));
	}

}
