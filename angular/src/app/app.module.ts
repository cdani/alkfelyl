import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NewsContainerComponent } from './news-container/news-container.component';
import { GreetingsComponent } from './greetings/greetings.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';
import { PersistanceService } from './persistance.service';
import { HomePageComponent } from './home-page/home-page.component';
import { ToDoListPageComponent } from './to-do-list-page/to-do-list-page.component';
import { LogInComponent } from './log-in/log-in.component';
import { LogInPageComponent } from './log-in-page/log-in-page.component';

// Router things
const appRoutes: Routes = [
  { path: 'home', component: HomePageComponent },
  { path: 'to-do-list', component: ToDoListPageComponent },
  { path: 'log-in', component: LogInComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NewsContainerComponent,
    GreetingsComponent,
    ToDoListComponent,
    HomePageComponent,
    ToDoListPageComponent,
    LogInComponent,
    LogInPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [PersistanceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
