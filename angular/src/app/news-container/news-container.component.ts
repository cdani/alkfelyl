import { Component } from '@angular/core';

@Component({
  selector: 'app-news-container',
  templateUrl: './news-container.component.html',
  styleUrls: ['./news-container.component.css']
})
export class NewsContainerComponent {

  news = [
    {
      title: 'Lore ipsum dolor...',
      author: 'Bela',
      content: 'sit amet ...'
    },
    {
      title: 'Lore dolor...',
      author: 'Bela',
      content: 'sit amsitet ...'
    },
    {
      title: 'Lore ipsum ...',
      author: 'Bela',
      content: 'sit sit ...'
    },
    {
      title: 'Lore ipsum ... dolor',
      author: 'Bela',
      content: 'amet amet ...'
    },
    {
      title: 'Lore ipsum dolor...',
      author: 'Bela',
      content: 'ametamet amet ...'
    },
    {
      title: 'Lore dolor...ipsum',
      author: 'Bela',
      content: 'sit amsitet ...'
    },
  ];
}
