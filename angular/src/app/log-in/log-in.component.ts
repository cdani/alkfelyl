import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Subscription } from 'rxjs/subscription';
import { PersistanceService } from '../persistance.service';

@Component({
  selector: 'log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit, OnDestroy {

  constructor(private persistanceService: PersistanceService) { }
	private subscription: Subscription;
	private users: {username: string, password: string}[];
  ngOnInit() {
    this.subscription = this.persistanceService.getAllUsers()
    .subscribe(users => this.users = users);
  }
  
  ngOnDestroy() {
    this.subscription.unsubscribe()
  }
  
  addUser(nameModel: NgModel, passwordModel: NgModel) {
    const user = {username: nameModel.value, password: passwordModel.value};

    this.users.push(user);
	console.log("ASIUDHASDHIOASDIOASNDOIASND");
	console.log(JSON.stringify(this.users));
	
	

    nameModel.reset();
	passwordModel.reset();
  }
  
  logIn(nameModel: NgModel, passwordModel: NgModel) {
	   var users = JSON.parse(localStorage.getItem('users'));
	   var l = false;
	   for(var i = 0; i < users.length; i++){
			if (users[i].username===nameModel.value) {
				l=true;
				if (users[i].password===passwordModel.value) {
					localStorage.setItem("currentUser",users[i].username);
					window.open('http://localhost:4200/to-do-list');
					break;
				}
				else {
					alert("RETadsasdasdARDÁLT VAGY!");
					passwordModel.reset();
					break;
				}
			}
	   }
	   
	   if (l==false) {
		   alert("NINCS ILYEN EMBER, INKÁBB REGISZTRÁLJ!!");
	   }
  }
  
  register (nameModel: NgModel, passwordModel: NgModel) {
	  var users = JSON.parse(localStorage.getItem('users'));
	  for (var i=0; i<users.length; i++) {
		  if (users[i].username===nameModel.value) {
			  alert("VÓTMÁ!");
			  nameModel.reset();
			  passwordModel.reset();
			  return -1;
		  }
	  }
	  const user = {username: nameModel.value, password: passwordModel.value};
	  nameModel.reset();
	  passwordModel.reset();
	  this.persistanceService.saveUsers(user);
	  alert("Sikeres regisztráció! :)");
  }
	  
  

}
