import { Component } from '@angular/core';

@Component({
  selector: 'app-greetings',
  templateUrl: './greetings.component.html',
  styleUrls: ['./greetings.component.css']
})
export class GreetingsComponent {
  _greetings = [
    'Jo reggelt!',
    'Good Morning!',
    'Guten Tag!',
    'Buenos dias!'
  ];

  greeting: string;

  constructor() {
    this._tick();
  }

  _tick() {
    const i = Math.round(Math.random() * (this._greetings.length - 1));
    this.greeting = this._greetings[i];

    setTimeout(this._tick.bind(this), 1000);
  }

}
