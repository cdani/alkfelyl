package edu.elte.hello.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import edu.elte.hello.model.User;
import java.util.Optional;

@Repository
public interface UserRepository
    extends CrudRepository<User, Integer> {

    Optional<User> findByUsername(String username);
}
