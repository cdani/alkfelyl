package edu.elte.hello.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class ToDo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public int id;
    
    public String name;
    public Boolean done;

    public static ToDo create(String name, Boolean done) {
        ToDo toDo = new ToDo();
        toDo.setName(name);
        toDo.setDone(done);

        return toDo;
    }
}
